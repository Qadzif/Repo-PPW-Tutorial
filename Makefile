makeweb: cleancompile makeenv preparedeploy run

makeenv:
	python -m venv env
	source env/bin/activate
	pip install -r requirements.txt

collectstatic:
	python manage.py collectstatic	

preparedatabse:
	python manage.py makemigrations
	python manage.py migrate

cleancompile:
	find . -name "*.pyc" -delete

run:
	python manage.py runserver

preparedeploy: collectstatic preparedatabse

